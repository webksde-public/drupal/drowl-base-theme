/**
 * @file
 * Truncates text by given selector and adds a "... read more" link after the truncated textblock.
 *
 */

(function ($, Drupal) {
  Drupal.behaviors.drowl_base_highlight_news = {
    attach: function (context, settings) {
      // News Bar on top of the page (closable Foundation Callout)
      const newsBars = context.querySelectorAll(".block--highlight-news");

      function hideNewsBar(newsBar, newsTimestamp, animated = false) {
        const newsBarBlockCloseButton = newsBar.querySelector(
          ".block--highlight-news__close"
        );
        const rootStyles = getComputedStyle(document.documentElement);
        const animationDuration = parseFloat(rootStyles.getPropertyValue('--animate-duration')) * 1000 || 1000;
        if (animated) {
          newsBar.classList.remove("fadeInDown");
          newsBar.classList.add("fadeOutUp");
          setTimeout(() => {
            newsBar.classList.add("d-none");
            newsBar.classList.remove("fadeOutUp");
          }, animationDuration);
        }else{
          newsBar.classList.add("d-none");
        }
        newsBarBlockCloseButton.setAttribute("aria-hidden", "true");
        if (newsTimestamp) {
          Cookies.set("highlight_news_timestamp", newsTimestamp);
        }
      }

      function showNewsBar(newsBar) {
        const newsBarBlockCloseButton = newsBar.querySelector(
          ".block--highlight-news__close"
        );
        newsBarBlockCloseButton.setAttribute("aria-hidden", "false");
        newsBar.classList.remove("d-none");
        newsBar.classList.add("fadeInDown");  // Hinzugefügt für Animation
      }

      newsBars.forEach((newsBarBlock) => {
        const newsTimestamp = newsBarBlock.querySelector(
          "[data-news-cookie-timestamp]"
        ).dataset.newsCookieTimestamp;
        const cookieNewsTimestamp = Cookies.get("highlight_news_timestamp");

        if (
          cookieNewsTimestamp === undefined ||
          newsTimestamp > cookieNewsTimestamp
        ) {
          showNewsBar(newsBarBlock);
        } else {
          hideNewsBar(newsBarBlock);
        }

        newsBarBlock
          .querySelectorAll("a, .block--highlight-news__close")
          .forEach((element) => {
            element.addEventListener("click", () => {
              hideNewsBar(newsBarBlock, newsTimestamp, true);
            });
          });
      });


      // Floating, animated News Badge Message
      const newsBadges = context.querySelectorAll(".news-badge");

      newsBadges.forEach((badge) => {
        const newsTimestamp = badge.dataset.newsCookieTimestamp;
        const cookieName =
          badge.dataset.newsCookieName || "highlight_news_badge_timestamp";
        const cookieNewsTimestamp = Cookies.get(cookieName);

        if (
          cookieNewsTimestamp === undefined ||
          newsTimestamp > cookieNewsTimestamp
        ) {
          badge.classList.remove("news-badge--hidden");
          badge.classList.add("news-badge--show");

          setTimeout(() => {
            badge.classList.add("news-badge--show-title");
          }, 1500);

          const newsBadgeHideTimeout = setTimeout(() => {
            badge.classList.remove("news-badge--show-title");
          }, 8000);

          badge.addEventListener("mouseenter", () => {
            clearTimeout(newsBadgeHideTimeout);
            badge.classList.add("news-badge--show-title");
          });

          badge.addEventListener("touchstart", (e) => {
            e.preventDefault();
            clearTimeout(newsBadgeHideTimeout);
            badge.classList.add("news-badge--show-title");
          });
        } else {
          badge.classList.add("news-badge--hidden");
          badge.classList.remove("news-badge--show");
        }

        badge
          .querySelector(".news-badge__icon")
          .addEventListener("click", () => {
            badge.classList.toggle("news-badge--show-title");
          });

        badge
          .querySelector(".news-badge__title")
          .addEventListener("pointerup", (e) => {
            if (e.target.classList.contains("news-badge__link")) {
              e.preventDefault();
            }

            badge.classList.remove("news-badge--show-title");
            Cookies.set(cookieName, newsTimestamp);

            setTimeout(() => {
              badge.classList.remove("news-badge--show");
              badge.classList.add("news-badge--hidden");
            }, 1000);
          });

        badge
          .querySelector(".news-badge__title a")
          .addEventListener("touchend", (e) => {
            window.location.href = e.target.href;
          });
      });
    },
  };
})(jQuery, Drupal);
