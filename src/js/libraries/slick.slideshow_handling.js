/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_slideshow_handling = {
    attach: function (context, settings) {
      var $slickSlideshows = $('.slick .slick-slider', context);
      var slideshowObserver;

      // A more simple approach to pause the slideshow if a (youtube) video inside is "clicked"
      // Source: https://gist.github.com/jaydson/1780598
      // TODO: Add HTML5 player and the new d8 remote player type also
      $('.slick .video-embed-field-provider-youtube', context).each(function () {
        var $videoPlayer = $(this);

        var myConfObj = {
          iframeMouseOver: false,
        };
        window.addEventListener('blur', function () {
          if (myConfObj.iframeMouseOver) {
            $videoPlayer.parents('.slick-slider:first').slick('slickSetOption', 'autoplay', false, true);
          }
        });
        $videoPlayer[0].addEventListener('mouseover', function () {
          myConfObj.iframeMouseOver = true;
        });
        $videoPlayer[0].addEventListener('mouseout', function () {
          myConfObj.iframeMouseOver = false;
        });
      });

      // Remove player controls from HTML5 Videos in .media-slide if the video is cropped by its wrapper
      $('.media-slide:not(.media-slide--media-size-original) video:first', context).removeAttr('controls');

      /**
       * Pause all slick sliders if scrolled out of viewport
       * @see {@link http://stackoverflow.com/a/14092859/1410103}
       */
      /**
       * Check each autoplay-slider and set autoplay to false while not in viewport.
       */
      function setSlickAutoplay(slider, stateToSet) {
        var { autoplay } = slider.slick.options;
        if (slider.slick !== null) {
          // Set data attribute on autoplay slideshows so we can differentiate between regular and autoplay slideshows.
          if (!slider.hasAttribute('data-slick-autoplay-if-visible') && autoplay === true) {
            $(slider).attr('data-slick-autoplay-if-visible', 'true');
          }
          if (slider.hasAttribute('data-slick-autoplay-if-visible')) {
            // console.log('slick set autoplay state: ' + stateToSet);
            if (stateToSet) {
              $(slider).slick('slickPlay');
            } else {
              $(slider).slick('slickPause');
            }
          }
        } else {
          // console.log('slideshowObserver tried to set a status on a unintialised slideshow.');
        }
      }

      slideshowObserver = new IntersectionObserver(function (slideshow) {
        $(slideshow).each(function () {
          if ($(this)[0].intersectionRatio > 0) {
            // Slideshow in view => play
            setSlickAutoplay($(this)[0].target, true);
          } else {
            // Slideshow out of view => stop
            setSlickAutoplay($(this)[0].target, false);
          }
        });
      });

      $slickSlideshows.each(function () {
        const $slider = $(this);
        const $sliderWrapper = $slider.parents('.slick:first');
        const $sliderDirectionNav = $sliderWrapper.children('.slick__arrow:first');
        // Observe slideshows
        slideshowObserver.observe($(this)[0]);
        $slider.on('init reInit setPosition', function () {
          // Add modifier class if center mode is active
          if ($slider[0].slick.options.centerMode) {
            $slider.addClass('slick-slider--center-mode');
          }
          // Add modifier class if slick arrows are positioned outside
          // and the viewport is < slick_arrow width.
          if($sliderWrapper.hasClass('slick--controlsoutside') || $sliderWrapper.parents().hasClass('slick-controls-outside')){
            const directionNavOutsideOverflowBehavoir = getComputedStyle($sliderWrapper[0]).getPropertyValue('--direction-nav-outside-fallback-behavoir').replace(/['"]+/g, '');
            if($sliderDirectionNav.outerWidth() > $('body:first').outerWidth()){
              $sliderWrapper.addClass('slick--controlsoutside--direction-nav-overflow-behavoir-' + directionNavOutsideOverflowBehavoir);
            }
          }
        });
        $slider.on('init', function () {
          // Obeserve
          slideshowObserver.observe($(this)[0]);
        });
      });
    },
  };
})(jQuery, Drupal);
