# Dropdown menu

A dropdown menu is a toggleable menu that allows the user to choose one value from a predefined list.
This is based on radix:dropdown-menu which doesn't adress deeper menu structures.

## Usage

A practical example is in the **Nav** component:

```twig
  {% include 'radix:dropdown-menu' with {
    items: item.below,
    direction: dropdown_direction|default('dropdown'),
    overview_link: alignment == 'vertical' ? item|without('below') : [],
    is_dropdown_toggle_static: alignment == 'vertical' or is_megamenu,
    is_hover_nav: is_hover_nav ?: false,
  } %}
```
Its very important to return an array to the overview_link, otherwise the SDC will return an error.
