## How to maintain the components directory
For a quick start you may copy/paste all the components or those you need from the parent radix theme
under `radix/src/components` into this directory (https://git.drupalcode.org/project/radix/-/tree/6.0.x/components?ref_type=heads)

Also make sure that you import the `init.scss` into your components SASS file, the `block` component has been moved into the subtheme as an example

Document your changes!

YML example comment (right on top):
# Changes against Radix:
# - No Schema changes

Twig example Comment (docblock):
 * Changes against Radix:
 * - Changed the page title classes in the twig file
