/**
 * @file
 * DROWL Paragraphs frontend JavaScript
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_background_media = {
    attach: function (context, settings) {
      if(document.querySelector('.bg-media-parallax')){
        // document.body.onload = castParallax();
        $(once("drowl-background-media", ".bg-media-parallax", context)).each(function () {
          new simpleParallax(this, {
            customWrapper: '.bg-media-parallax'
          });
        });
      }
    },
  };
})(jQuery, Drupal);
