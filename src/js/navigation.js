(function ($, Drupal) {
  /**
   * General fixes for Drupal core bugs
   */
  Drupal.behaviors.drowl_base_navigation = {
    attach: function (context, settings) {
      // TODO: Move to seperate library "drowl_base:fixed-navbar" and attach it only if placement ist not set to '' or 'static'
      // => when fixed remove the !$pageNavbar.classList.contains('static')

      // Add classes when the page navbar becomes stuck
      const $pageNavbar = document.querySelector('.page__navbar');
      if($pageNavbar && !$pageNavbar.classList.contains('static')){
        const topbarTopOffset = $pageNavbar.offsetTop;
        const isPlacementFixedAfterScroll = $pageNavbar.classList.contains('fixed-after-scroll');
        // If the placement option 'fixed-after-scroll' is set, we need to reserve the height of
        // the unstucked topbar and set it to the 'navbar-height-wrapper'.
        if(isPlacementFixedAfterScroll){
          const $pageNavbarHeightWrapper = $pageNavbar.parentNode;
          $pageNavbarHeightWrapper.style.minHeight = $pageNavbar.offsetHeight + 'px';
        }
        window.addEventListener("scroll", Drupal.drowl_base.functions.debounce(function () {
          if (window.scrollY > topbarTopOffset) {
            document.querySelector('body').classList.add('page-navbar-is-stuck');
            $pageNavbar.classList.add('page__navbar--is-stuck');
            // Add support for our custom value 'fixed-after-scroll' (wich is the drowl_base default)
            if(isPlacementFixedAfterScroll){
              $pageNavbar.classList.add('fixed-top');
            }
          }else{
            document.querySelector('body').classList.remove('page-navbar-is-stuck');
            $pageNavbar.classList.remove('page__navbar--is-stuck');
            // Add support for our custom value 'fixed-after-scroll' (wich is the drowl_base default)
            if(isPlacementFixedAfterScroll){
              $pageNavbar.classList.remove('fixed-top');
            }
          }
        }, 100));
      }
      // Add body class if the offcanvas mobile menu is open
      // TODO: loop the offcanvas menus and add a generic class to the body
      document.querySelector('#offcanvas-main-nav').addEventListener('show.bs.offcanvas', function() {
        document.body.classList.add('offcanvas-main-nav-shown');
      });

      document.querySelector('#offcanvas-main-nav').addEventListener('hide.bs.offcanvas', function() {
        document.body.classList.remove('offcanvas-main-nav-shown');
      });
    }
  };
})(jQuery, Drupal);



