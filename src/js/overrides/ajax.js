/**
 * @file
 * Defines ajax throbber theme functions.
 * Credit to: Pierre Dureau (pdureau) for the initial code.
 */

((Drupal) => {
  /**
   * An animated progress throbber and container element for AJAX operations.
   *
   * @param {string} [message]
   *   (optional) The message shown on the UI.
   * @return {string}
   *   The HTML markup for the throbber.
   */

  // TODO: Check if the radix override looks fine, so we can remove our old code => also remove the CSS .sk-circle ...
  // Our old custom implementation:
  // Drupal.theme.ajaxProgressThrobber = function () {
  //   var throbberMarkup = `<div class='ajax-spinner ajax-spinner--inline'>
  //       <div class='spinner spinner--small'>
  //         <div class='sk-circle'>
  //           <div class='sk-circle1 sk-child'></div>
  //           <div class='sk-circle2 sk-child'></div>
  //           <div class='sk-circle3 sk-child'></div>
  //           <div class='sk-circle4 sk-child'></div>
  //           <div class='sk-circle5 sk-child'></div>
  //           <div class='sk-circle6 sk-child'></div>
  //           <div class='sk-circle7 sk-child'></div>
  //           <div class='sk-circle8 sk-child'></div>
  //           <div class='sk-circle9 sk-child'></div>
  //           <div class='sk-circle10 sk-child'></div>
  //           <div class='sk-circle11 sk-child'></div>
  //           <div class='sk-circle12 sk-child'></div>
  //         </div>
  //       </div>
  //       <span class='ajax-spinner__label visually-hidden'>
  //         ${Drupal.t('Loading&nbsp;&hellip;', {}, { context: 'Loading text for Drupal cores Ajax throbber (inline)' })}
  //       </span>
  //     </div>`;
  //   return throbberMarkup;
  // };

  /**
   * An animated progress throbber and container element for AJAX operations.
   *
   * @return {string}
   *   The HTML markup for the throbber.
   */
  Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
    var throbberMarkup = `<div class='ajax-spinner ajax-spinner--fullscreen'>
        <div class='spinner'>
          <div class='double-bounce1'></div>
          <div class='double-bounce2'></div>
        </div>
        <span class='ajax-spinner__label'>
          ${Drupal.t(
            'Loading&nbsp;&hellip;',
            {},
            { context: 'Loading text for Drupal cores Ajax throbber (fullscreen)' },
          )}
        </span>
      </div>`;
    return throbberMarkup;
  };

  Drupal.theme.ajaxProgressThrobber = (message) => {
    // Build markup without adding extra white space since it affects rendering.
    const messageMarkup =
      typeof message === 'string'
        ? Drupal.theme('ajaxProgressMessage', message)
        : '';

    if (messageMarkup === '') {
      const defaultMessage = Drupal.t('Loading...');
      return `<div class="ajax-progress ajax-progress-throbber">
        <div class="spinner-border spinner-border-sm" role="status">
          <span class="visually-hidden">${defaultMessage}</span>
        </div>
      </div>`;
    }

    return `<div class="ajax-progress ajax-progress-throbber">
      <span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
      ${messageMarkup}
    </div>`;
  };

  /**
   * Formats text accompanying the AJAX progress throbber.
   *
   * @param {string} message
   *   The message shown on the UI.
   * @return {string}
   *   The HTML markup for the throbber.
   */
  Drupal.theme.ajaxProgressMessage = (message) =>
    `<span role="status">${message}</span>`;
})(Drupal);
