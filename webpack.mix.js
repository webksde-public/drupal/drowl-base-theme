/*
 |--------------------------------------------------------------------------
 | Project configuration
 |--------------------------------------------------------------------------
*/

const config = require('./webpack.mix.config.js');
// npm script commandline flag to enable the rsync script and
// sync the files with the configured webserver.
const syncToWebserver = process.env.npm_config_upload;
const buildAdminPreviewStyles = process.env.npm_config_adminpreview;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
*/
require('dotenv').config({ path: '.env.local' });
const mix = require('laravel-mix');
const chokidar = require('chokidar');
const { spawn } = require('child_process');
const glob = require('glob');
// We need a custom watcher as trigger for the rsync script, it has
// nothing todo with the leveral mix build tasks.
// The custom watcher has only to watch files which are not
// addressed by any of the webpack mix tasks.
let watcher;
if(syncToWebserver){
  const watcher_files = [
    '**/*.twig',
    '**/*.php',
    '**/*.theme',
    '**/*.inc',
    '**/*.yml',
    './svg',
    './png'
  ]
  watcher = chokidar.watch(watcher_files, {
    ignoreInitial: true,
    ignored: [
      // Exclude the specific directories from the sync file watcher.
      // Also exclude the directories/files which are already watched
      // by the webpack mix default watcher (addressed by the tasks
      // defined beyond).
      '.git',
      'node_modules',
      'src',
    ],
  });
}
require('laravel-mix-stylelint');
require('laravel-mix-copy-watched');

/*
  |--------------------------------------------------------------------------
  | Configuration
  |--------------------------------------------------------------------------
*/

mix
	.sourceMaps()
	.webpackConfig({
		// Use the jQuery shipped with Drupal to avoid conflicts.
		externals: {
			jquery: 'jQuery',
		},
		devtool: 'source-map',
	})
  // See: https://laravel-mix.com/docs/6.0/os-notifications
	// .disableNotifications()
	.options({
		processCssUrls: false,
	});

/*
  |--------------------------------------------------------------------------
  | Copy Libraries from NPM
  |--------------------------------------------------------------------------
*/

mix.copyDirectory('node_modules/hoverintent/dist', 'libraries/hoverintent');

/*
  |--------------------------------------------------------------------------
  | Functions
  |--------------------------------------------------------------------------
*/

function remoteSync(remoteSyncRunning){
  const rsync = spawn('rsync', [
    '-avz',
    '--delete',
    '-e',
    `ssh -p ${config.port}`, // Use template literal (backticks)
    './',
    `${config.user}@${config.host}:${config.remotePath}`,
    '--exclude-from',
    './webpack.mix.config.sync_excludes.txt',
  ]);
  rsync.stdout.on('data', (data) => {
    console.log(`stdout: ${data.toString()}`);
  });

  rsync.stderr.on('data', (data) => {
    console.error(`stderr: ${data.toString()}`);
  });

  rsync.on('close', (code) => {
    if (code !== 0) {
      console.error("\x1b[33m", `⚠ RSYNC exited with error code ${code}`, "\x1b[30m");
      // Handle the error, e.g., throw an exception, notify the user, etc.
    } else {
      console.log("\x1b[32m", `✔ RSYNC exited successfully`, "\x1b[30m");
    }
    return remoteSyncRunning = false;
  });
}

/*
  |--------------------------------------------------------------------------
  | Browsersync
  |--------------------------------------------------------------------------
*/
// mix.browserSync({
// 	proxy: process.env.DRUPAL_BASE_URL,
// 	files: [
// 		'components/**/*.css',
// 		'components/**/*.js',
// 		'components/**/*.twig',
// 		'templates/**/*.twig',
// 	],
// 	stream: true,
// });

/*
  |--------------------------------------------------------------------------
  | SASS
  |--------------------------------------------------------------------------
*/

// TODO: We better loop this. Figure out how to exclude "_x.scss" files with
// webpack mix.

// for (const sourcePath of glob.sync("**/*.scss")) {
// 	if(!sourcePath.includes("components/")){
// 		mix.sass(sourcePath, "build/css");
// 	}
// }

// Bootstrap
mix.sass("src/scss/drowl_base.bootstrap.scss", "build/css/drowl_base.bootstrap.css");
// Base
mix.sass("src/scss/base/drupal_tweaks.scss", "build/css/base/drupal_tweaks.css");
mix.sass("src/scss/base/bootstrap_tweaks.scss", "build/css/base/bootstrap_tweaks.css");
mix.sass("src/scss/base/elements.scss", "build/css/base/elements.css");
mix.sass("src/scss/base/helpers.scss", "build/css/base/helpers.css");
mix.sass("src/scss/base/typography.scss", "build/css/base/typography.css");
mix.sass("src/scss/base/utilities.scss", "build/css/base/utilities.css");
mix.sass("src/scss/base/css_variables.scss", "build/css/base/css_variables.css");
// Components
mix.sass("src/scss/components/alerts.scss", "build/css/components/alerts.css");
mix.sass("src/scss/components/box-styles.scss", "build/css/components/box-styles.css");
mix.sass("src/scss/components/buttons.scss", "build/css/components/buttons.css");
mix.sass("src/scss/components/cards.scss", "build/css/components/cards.css");
mix.sass("src/scss/components/icons.scss", "build/css/components/icons.css");
mix.sass("src/scss/components/list-styles.scss", "build/css/components/list-styles.css");
mix.sass("src/scss/components/media-objects.scss", "build/css/components/media-objects.css");
mix.sass("src/scss/components/misc.scss", "build/css/components/misc.css");
mix.sass("src/scss/components/slider.scss", "build/css/components/slider.css");
mix.sass("src/scss/components/tabs-accordion.scss", "build/css/components/tabs-accordion.css");
// Drupal
mix.sass("src/scss/drupal/admin-toolbar.scss", "build/css/drupal/admin-toolbar.css");
mix.sass("src/scss/drupal/core.autocomplete.scss", "build/css/drupal/core.autocomplete.css");
mix.sass("src/scss/drupal/core.dialog.scss", "build/css/drupal/core.dialog.css");
mix.sass("src/scss/drupal/maintenance-page.scss", "build/css/drupal/maintenance-page.css");
mix.sass("src/scss/drupal/search-page.scss", "build/css/drupal/search-page.css");
// Entities
mix.sass("src/scss/entities/blocks.scss", "build/css/entities/blocks.css");
mix.sass("src/scss/entities/fields.scss", "build/css/entities/fields.css");
mix.sass("src/scss/entities/media.scss", "build/css/entities/media.css");
mix.sass("src/scss/entities/misc.scss", "build/css/entities/misc.css");
mix.sass("src/scss/entities/nodes.scss", "build/css/entities/nodes.css");
mix.sass("src/scss/entities/paragraphs.scss", "build/css/entities/paragraphs.css");
mix.sass("src/scss/entities/tokens.scss", "build/css/entities/tokens.css");
mix.sass("src/scss/entities/views.scss", "build/css/entities/views.css");
// User
mix.sass("src/scss/user/user.scss", "build/css/user/user.css");
mix.sass("src/scss/user/user-login.scss", "build/css/user/user-login.css");
// Forms
mix.sass("src/scss/forms/misc.scss", "build/css/forms/misc.css");
// Layouts
mix.sass("src/scss/layouts/misc.scss", "build/css/layouts/misc.css");
// Libraries
mix.sass("src/scss/libraries/drowl_base.background-media.scss", "build/css/libraries/drowl_base.background-media.css");
mix.sass("src/scss/libraries/drowl_base.expandable-filters.scss", "build/css/libraries/drowl_base.expandable-filters.css");
mix.sass("src/scss/libraries/drowl_base.highlight-news.scss", "build/css/libraries/drowl_base.highlight-news.css");
mix.sass("src/scss/libraries/photoswipe.scss", "build/css/libraries/photoswipe.css");
mix.sass("src/scss/libraries/slick.scss", "build/css/libraries/slick.css");
// Modules
mix.sass("src/scss/modules/blazy.media.scss", "build/css/modules/blazy.media.css");
mix.sass("src/scss/modules/captcha.scss", "build/css/modules/captcha.css");
mix.sass("src/scss/modules/cookies.banner.scss", "build/css/modules/cookies.banner.css");
mix.sass("src/scss/modules/cookies.variables.scss", "build/css/modules/cookies.variables.css");
mix.sass("src/scss/modules/webform-help.scss", "build/css/modules/webform-help.css");
mix.sass("src/scss/modules/webform.scss", "build/css/modules/webform.css");
// Navigation
mix.sass("src/scss/navigation/language-switcher.scss", "build/css/navigation/language-switcher.css");
mix.sass("src/scss/navigation/mega-menu.scss", "build/css/navigation/mega-menu.css");
mix.sass("src/scss/navigation/misc.scss", "build/css/navigation/misc.css");
mix.sass("src/scss/navigation/nav.scss", "build/css/navigation/nav.css");
mix.sass("src/scss/navigation/navbar.scss", "build/css/navigation/navbar.css");
// Page
mix.sass("src/scss/page/content.scss", "build/css/page/content.css");
mix.sass("src/scss/page/footer.scss", "build/css/page/footer.css");
mix.sass("src/scss/page/header.scss", "build/css/page/header.css");
mix.sass("src/scss/page/regions.scss", "build/css/page/regions.css");
// Iconset
mix.sass("src/scss/drowl_base.iconset.scss", "build/css/drowl_base.iconset.css");
// Animations
mix.sass("src/scss/animations/drowl_base.animations.scss", "build/css/animations/drowl_base.animations.css");
mix.sass("src/scss/animations/drowl_base.spinners.scss", "build/css/animations/drowl_base.spinners.css");

// DROWL Base Demo Styles
mix.sass("src/scss/drowl_base.style.scss", "build/css/drowl_base.style.css");
// DROWL Base Mail Styles (replaced by DROWL Child)
mix.sass("src/scss/mail.scss", "build/css/mail.css");

for (const sourcePath of glob.sync("components/**/*.scss")) {
	const destinationPath = sourcePath.replace(/\.scss$/, ".css");
	mix.sass(sourcePath, destinationPath);
}

// Build CKEditor preview styles.
// We dont do this all the time, to not slow down the regular build
// process, when its good enough to run this once anything relevant
// has changed.
if(buildAdminPreviewStyles){
  mix.sass("src/scss/modules/drowl.paragraphs-preview-styles.scss", "build/css/modules/drowl.paragraphs-preview-styles.css").after(stats => {
    if(syncToWebserver){
      remoteSync();
    }
  });
}

/*
  |--------------------------------------------------------------------------
  | JS
  |--------------------------------------------------------------------------
*/

// Workaround for unwanted webpack comment stuff in the first compiled javascript file (dev and production)
mix.js("src/js/webpack/crap.workaround.js", "build/js/webpack/crap.workaround.js");
// Drupal Fixes / Improvements
mix.js("src/js/drupal/core.fixes.js", "build/js/drupal/core.fixes.js");
mix.js("src/js/drupal/user.edit.js", "build/js/drupal/user.edit.js");
// Drupal Overrides
mix.js("src/js/overrides/active-link.js", "build/js/overrides/active-link.js");
mix.js("src/js/overrides/ajax.js", "build/js/overrides/ajax.js");
mix.js("src/js/overrides/checkbox.js", "build/js/overrides/checkbox.js");
mix.js("src/js/overrides/dialog.ajax.js", "build/js/overrides/dialog.ajax.js");
mix.js("src/js/overrides/dialog.js", "build/js/overrides/dialog.js");
mix.js("src/js/overrides/message.js", "build/js/overrides/message.js");
mix.js("src/js/overrides/progress.js", "build/js/overrides/progress.js");
mix.js("src/js/overrides/validate.js", "build/js/overrides/validate.js");
// Libraries
mix.js("src/js/libraries/attention_seeker_fly_ins.js", "build/js/libraries/attention_seeker_fly_ins.js");
mix.js("src/js/libraries/background_media.js", "build/js/libraries/background_media.js");
mix.js("src/js/libraries/highlight_news.js", "build/js/libraries/highlight_news.js");
mix.js("src/js/libraries/slick.slideshow_handling.js", "build/js/libraries/slick.slideshow_handling.js");
// DROWL Base
mix.js("src/js/functions.js", "build/js/functions.js");
mix.js("src/js/helpers.js", "build/js/helpers.js");
mix.js("src/js/navigation.js", "build/js/navigation.js");
// Bootstrap
mix.js("src/js/bootstrap/init.js", "build/js/bootstrap/init.js");

for (const sourcePath of glob.sync("components/**/_*.js")) {
	const destinationPath = sourcePath.replace(/\/_([^/]+\.js)$/, "/$1");
	mix.js(sourcePath, destinationPath);
}

/*
  |--------------------------------------------------------------------------
  | Style Lint
  |--------------------------------------------------------------------------
*/
mix.stylelint({
	configFile: './.stylelintrc.json',
	context: './src',
	failOnError: false,
	files: ['**/*.scss'],
	quiet: false,
	customSyntax: 'postcss-scss',
});

/*
  |--------------------------------------------------------------------------
  * IMAGES / ICONS / VIDEOS / FONTS
  |--------------------------------------------------------------------------
  */
// * Directly copies the images, icons and fonts with no optimizations on the images
mix.copyDirectoryWatched('src/assets/images', 'build/assets/images');
mix.copyDirectoryWatched('src/assets/favicons', 'build/assets/favicons');
mix.copyDirectoryWatched('src/assets/icons', 'build/assets/icons');
mix.copyDirectoryWatched('src/assets/videos', 'build/assets/videos');
mix.copyDirectoryWatched('src/assets/fonts/**/*', 'build/fonts').after(stats => {
  // Use the last task to trigger the webserver sync
  if(syncToWebserver){
    remoteSync();
  }
});

/*
  |--------------------------------------------------------------------------
  | Sync changes with the webserver usind the "--upload" flag
  | on the mix watch task.
  |--------------------------------------------------------------------------
*/

// Custom watcher for files which aren't addressed by the above tasks.
if(syncToWebserver){
  watcher.on('all', (event, path) => {
    // To not trigger the remotesync multiple times, when multiple
    // files are changed in a short time period, we block a re-run
    // by setting a flag.
    if (event !== 'addDir' && event !== 'unlinkDir') { // Ignore directory changes
      // TODO: VL add color to this message
      console.log("\x1b[33m", `⇆ File changed: ${path} => trigger remote sync`, "\x1b[30m");
      remoteSync();
    }
  });
}
