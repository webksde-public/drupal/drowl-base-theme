# DROWL Base 2.0 (Bootstrap based)

[TOC]

## Helpful documentation
- [Radix (Base Theme) Docs](https://doxigo.gitbook.io/radix)
  - [Radix CLI](https://github.com/doxigo/drupal-radix-cli)
- [Drupal Single Directory Components (SDC) Docs](https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/)
- [Bootstrap 5 Docs](https://getbootstrap.com/docs/5.3)
- [Laravel Mix (Build Pipeline Library) Docs](https://laravel-mix.com/docs)

## Guidelines

- Follow the Drupal Guidelines: https://www.drupal.org/docs/develop/theming-drupal
- Use Components (SDC) for all reusable tempalte snippets
- With Bootstrap as a framework, we have to deal with two different conventions when it comes to CSS class naming. Drupal prefers BEM (https://getbem.com/naming), while Bootstrap uses some kind of OOCSS. We use this to our advantage, and use BEM for our custom page structure and components, so that we can better differentiate them. If it makes sense to use BEM classes next to Bootstrap classes, just do it. Example: 'page__navar navbar' are fine.

## Extend Bootstrap

### Components

#### Grid
- gy-equal-x: Adds vertical gutter equal to the horizontal gutter

#### Buttons
- btn-width-full: Stretches a button to 100% width (like the old btn-block class)

#### Icons
DROWL Base brings a set of basic icons which are available as SASS and PHP variables.
In sass the icons are named $ico-theme-X (see drowl-sass-base variables).
In PHP the icons are available by the global drowl_theme_icons array.

To override SASS variables, see $basics-frontend-iconfont-family and the single $ico-theme-X variables.

To override single PHP variables:
`$variables['drowl_theme_icons']['info'] = 'ico-info-circle';`
or override all:
```php
$variables['drowl_theme_icons'] = [
  'info' => 'ico-info',
  'warning' => 'ico-warning',
  ...
];
```

## Helpers

### Body classes

- current-breakpoint-is-x: Current active breakpoint determined by JS.
- current-breakpoint-is-smaller-x: If the current breakpoint is smaller brekpoint x.
- current-breakpoint-is-larger-x: If the current breakpoint is larger brekpoint x.
- current-vw-is-smaller-container-max: Set if the current viewport width is smaller as the BS .container (max. page width)

## Utilities

### Text
- text-muted: Deprecated since BS 5.3 and replaced by a meaningless class. So we reinvented it.

### Javascript

#### Debounce / Throttle
```javascript
Drupal.drowl_base.functions.debounce(function,delay);

// Example usage:
new ResizeObserver(Drupal.drowl_base.functions.debounce(function(){
  // Do stuff!
}, 600));
```
#### Get current Breakpoint
```javascript
// Returns: Breakpoint Name (string), example: "lg"
Drupal.drowl_base.functions.getCurrentBreakpoint();
```

#### Get Scrollbar width
```javascript
// Returns: Pixel value (decimal), example: 15
Drupal.drowl_base.functions.getScrollbarWidth();
```

#### Events

##### Listen to viewport size changes
```javascript
document.addEventListener(
  "drowl_base:resize",
  (e) => {
    /* … */
  },
  false,
);
```

## SDC Single Directory Components

### Component Usage

Basically you have two ways to add a component:

#### Include

Include a component, and drop some parameters, but dont change its contents.

```twig
{% include 'drowl_base:block' with {
  html_tag: 'section',
  block_utility_classes: [
    'my-3',
    'block--local-tasks',
  ],
}%}
```

#### Embed

Include a component and change its contents using block overrides.

```twig
{% embed 'drowl_base:metaheader' with {
  color: 'light',
  navbar_utility_classes: ['bg-light'],
} %}
  {% block left %}
    Replacement
  {% endblock %}

  {% block right %}
    Replacement
  {% endblock %}
{% endembed %}
```

### Create new Components

Prefer to use drush genrate:
`drush generate theme:sdc:component`

or see: https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/creating-a-single-directory-component

### Override existing Components

> Only themes can override components (modules cannot override). For a theme to override a component, use the replaces key within the mytheme.component.yml file.

[Check SDC Docs for details](https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/creating-a-single-directory-component#s-overriding-components-provided-by-other-modules-or-themes)

## Override DROWL Base Stylesheets

All stylesheets are written with CSS variables (where possible). So in most cases, you might only need to modify your SASS variables (so your drowl_child.variables.css file is updated).

If you need to modify or remove DROWL Base styles, you could simply remove or override the according Drupal library.

## Libraries

### Slick

DROWL Base has its own Slick Carousel theme, with various tweaks using CSS Variables.

#### Configuration variables

```scss
  --grid-gutter: var(--bs-grid-gutter);
  --direction-nav-top: calc(50% - (var(--dot-nav-reserved-outer-space) * 0.5));
  --direction-nav-item-width: var(--icon-size-lg);
  --direction-nav-item-height: var(--icon-size-lg);
  --direction-nav-icon-size: var(--icon-size-md);
  --direction-nav-item-bg: var(--bs-light-glass);
  --direction-nav-item-color: var(--bs-dark-glass);
  --direction-nav-item-bg-hover: var(--bs-light);
  --direction-nav-item-color-hover: var(--bs-dark);
  --dot-nav-item-width: var(--icon-size-xs);
  --dot-nav-item-height: var(--icon-size-xs);
  --dot-nav-item-radius: 50%;
  --dot-nav-grid-gutter: .35rem;
  --dot-nav-item-color: var(--bs-light-glass);
  --dot-nav-item-color-hover: var(--bs-primary);
  --dot-nav-item-color-active: var(--bs-white);
  // Dot nav alignment, available options: start, center, end.
  --dot-nav-alignment: center;
```
Check libraries/slick.scss for more details.

#### Controls outside

To show direction + dot nav outside the contents, use the class `slick-controls-outside` on any wrapper, or .slick,
`slick--controlsoutside` right on the .slick wrapper (prefered).

If slick__nav becomes > viewport width, it switches to the style defined by the CSS variable --direction-nav-outside-fallback-behavoir (could be: 'overlay','inset' or 'hide').

#### Controls inset

To show direction + dot nav outside the contents, *while not moving the direction nav outside the container*, use the class `slick-controls-inset` on any wrapper, or .slick, `slick--controlsinset` right on the .slick wrapper (prefered).

See libraries/slick.scss for details.

#### Allow Overflow

For slideshows which needs visible overflow (eg: because you have elements with box-shadow inside), you can set `slick-allow-overflow` on any wrapper. Its *very important* to set `slick-allow-overflow-parent` on the section
wrapper, to prevent the horizonal overflow. Otherwise the document will become horizontally scrollable.
