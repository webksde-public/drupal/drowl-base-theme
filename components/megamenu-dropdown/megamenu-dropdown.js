// TODO:
// - we get a o.call is not a function when leaving the trigger
// - this seems to complicated. Can we archive the same result with a simpler
//   approach?
//   - We can also add a backdrop as seperate element
//   - Also setting a body class makes totally sense
//   - Instead a seperate megamenu-background element we should simply animate
//     the megamenu flyout itself. BUT, we should try to prevent, the "out-animation"
//     till the the mouse leaves the nav completely, to prevent ugly jumping while
//     switching from one megamenu trail to another.
//     The body class should be usefull for this.
//   - Skip all the height stuff here, simply animate a very high max-height
//     (like 100vh - topbarHeight)

// const megamenuParentListItems = document.querySelectorAll('.navbar-nav .nav-item--mega-menu');
// const isTouch = 'ontouchstart' in window || !!navigator.msMaxTouchPoints;

// const openMegamenu = (bgElem) => {
//   document.body.classList.add('megamenu-visible');
// };

// const closeMegamenu = (bgElem) => {
//   document.body.classList.remove('megamenu-visible');
// };
// const megamenuContentElements = document.querySelectorAll('.navbar-nav .megamenu-flyout');

// document.getElementById('megamenu-dim').addEventListener('click touchstart', (e) => {
//   if (document.body.classList.contains('megamenu-visible')) {
//     e.preventDefault();
//     menuParentListItems.forEach((item) => {
//       item.classList.remove('is-open');
//     });
//     closeMegamenu(megamenuBackground, 0);
//   }
// });
