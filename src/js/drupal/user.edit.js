(function ($, Drupal) {
  /**
   * Tweaks to the user edit form
   */
  Drupal.behaviors.drowl_base_user_edit = {
    attach: function (context, settings) {
      // Show the current password field only if the user types into the e-mail or password field.
      // Show the confirm password field, only if the primary password field has changed.
      const currentPassFieldItem = document.querySelector('.user-form .form-item-current-pass');
      const mailAddressFieldItem = document.querySelector('.user-form .form-item-mail');
      const passwordFieldItem = document.querySelector('.user-form .form-item-pass-pass1');
      const passwordRepeatItem = document.querySelector('.user-form .form-item-pass-pass2');

      // Hide till the user changes a relevant field
      currentPassFieldItem.classList.add('d-none');
      passwordRepeatItem.classList.add('d-none');

      mailAddressFieldItem.querySelector('input').addEventListener('input', function(){
        currentPassFieldItem.classList.remove('d-none');
      });
      passwordFieldItem.querySelector('input').addEventListener('input', function(){
        currentPassFieldItem.classList.remove('d-none');
        passwordRepeatItem.classList.remove('d-none');
      });

    },
  };
})(jQuery, Drupal);
