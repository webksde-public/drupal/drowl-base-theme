(function ($, Drupal) {
  /**
   * General fixes for Drupal core bugs
   */
  Drupal.behaviors.drowl_base_core_fixes = {
    attach: function (context, settings) {
      // Implement Javascript empty check - here is the WHY:
      // - https://www.drupal.org/node/953034 (not solved for years and probably never will be)
      // - There are workarounds for many, but not all, cases - this class is for unsolvable ones.
      // - Some workarounds will destroy Drupals caching, see: https://www.drupal.org/docs/8/api/render-api/cacheability-of-render-arrays
      $('.js-empty-check', context).each(function () {
        if ($(this).children().length) {
          $(this).removeClass('js-empty-check');
        } else {
          // Entirely remove the element so it doesnt affect CSS :first-child/:nth selectors
          $(this).replaceWith(
            '<!-- DROWL BASE Theme | js-empty-check removed element: ' + $(this).attr('class') + ' -->',
          );
        }
      });
    },
  };
})(jQuery, Drupal);
