<?php

/**
 * @file
 * Add custom theme settings to the Radix sub-theme.
 *
 * Remember to add default values to config/install/drowl_base.settings.yml.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function drowl_base_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  // Add alt + title field to the regular logo field.
  $form['logo']['theme_logo_alt'] = [
    '#type' => 'textfield',
    '#title' => t('Logo alt attribute'),
    '#description' => t('Sets the logo alt attribute for SEO. <strong>Leave empty (default) to use the site name as alt text.</strong>'),
    '#default_value' => theme_get_setting('theme_logo_alt'),
  ];
  $form['logo']['theme_logo_title'] = [
    '#type' => 'textfield',
    '#title' => t('Logo title attribute'),
    '#description' => t('Sets the logo title attribute for SEO. <strong>Leave empty (default) to use the site name as title text.</strong>'),
    '#default_value' => theme_get_setting('theme_logo_title'),
  ];
  // Add upload field for a small version of the customer logo - usecase eg.:
  // The docked menu bar.
  $form['theme_logo_small'] = [
    '#type' => 'details',
    '#title' => t('Smaller Logo (a tiny variation of the logo)'),
    '#weight' => 8,
    '#open' => TRUE,
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small_use_default'] = [
    '#type' => 'checkbox',
    '#title' => t('Use the logo supplied by the theme'),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small_use_default'),
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small_path'] = [
    '#type' => 'textfield',
    '#title' => t('Path to custom logo (small)'),
    '#description' => t('Examples: logo.svg (for a file in the public filesystem), public://logo.svg, or themes/custom/drowl_child/logo.svg.'),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small_path'),
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small'] = [
    '#title' => t('Logo (small) upload'),
    '#type' => 'managed_file',
    '#description' => t("If you don't have direct file access to the server, use this field to upload your logo."),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small'),
    '#upload_location' => 'public://branding/',
  ];

  // Furthermore - move logo and favicon to bottom also.
  $form['logo']['#weight'] = 7;
  $form['favicon']['#weight'] = 9;
}
