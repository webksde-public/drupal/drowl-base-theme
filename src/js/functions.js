/**
 * @file
 * Themes custom functions collection.
 *
 */
(function ($, Drupal) {
  // Workaround: Ensure Drupal.drowl_base exists because in some situations
  // this file is loaded before drowl_base/drowl_base - nobody (TF&JP) knows why...
  if (!Drupal.drowl_base) {
    Drupal.drowl_base = {};
  }
  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.functions) {
    Drupal.drowl_base.functions = {};
  }

  /**
   * Select fake placeholders:
   * - Colorize the the current selected option to the default placeholder color
   *   while the first option is selected.
   *
   * Initialize by class / by adding the needed additonal wrapper, example:
   * $('.form-select--fake-placeholder', context).selectFakePlaceholder();
   */
  // TODO: Fix thix function namespace according to our standards: Drupal.drowl_base.functions.selectFakePlaceholder
  //       & replace it in drowl_base.global.js
  $.fn.selectFakePlaceholder = function () {
    $(this).each(function () {
      var $select = $(this);
      if ($select.children().first().is(':selected')) {
        $select.addClass('select-placeholder-option-active');
      } else {
        $select.removeClass('select-placeholder-option-active');
      }
      $select.on('change', function () {
        if ($(this).children().first().is(':selected')) {
          $(this).addClass('select-placeholder-option-active');
        } else {
          $(this).removeClass('select-placeholder-option-active');
        }
      });
    });
  };


  /**
   * Debounce function for throtteling expensive operations like resize or scroll.
   */
  Drupal.drowl_base.functions.debounce = function (f, delay) {
    let timer = 0;
    return function(...args) {
        clearTimeout(timer);
        timer = setTimeout(() => f.apply(this, args), delay);
    }
  };

  /**
   * Get current (Bootstrap) Breakpoint name.
   */
    Drupal.drowl_base.functions.getCurrentBreakpoint = function () {
      return getComputedStyle(document.querySelector('body')).getPropertyValue('--breakpoint-current').replace(/['"]+/g, '').trim();
    };

  /**
    * Set breakpoint helper classes, so we can use those instead
    * media queries inside our drowl_base and module styles.
    * Important: This is required by various DROWL module and theme stylesheets.
   */
  Drupal.drowl_base.functions.addBreakpointBodyClasses = function () {
    const body = document.querySelector('body');
    const bodyClasses = [];
    const breakpoints = ['sm', 'md', 'lg', 'xl', 'xxl'];
    let currentBreakpoint = Drupal.drowl_base.functions.getCurrentBreakpoint();

    for (let i = 0; i < breakpoints.length; i++) {
      if (i === breakpoints.indexOf(currentBreakpoint)) {
        bodyClasses.push(`current-breakpoint-is-${breakpoints[i]}`);
      } else if (i < breakpoints.indexOf(currentBreakpoint)) {
        bodyClasses.push(`current-breakpoint-is-larger-${breakpoints[i]}`);
      } else if (i > breakpoints.indexOf(currentBreakpoint)) {
        bodyClasses.push(`current-breakpoint-is-smaller-${breakpoints[i]}`);
      }
    }
    body.classList.add(...bodyClasses);
  };

  /**
   * Set the scrollbar width of the current browser
   * For the width calculation in our viewport-width
   * mixin / formatter class.
   * Original source: https://stackoverflow.com/a/13382873/6266306
   */
  Drupal.drowl_base.functions.getScrollbarWidth = function () {
    // Creating invisible container
    const outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    const inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);

    return scrollbarWidth;
  };
})(jQuery, Drupal);
